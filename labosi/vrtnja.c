#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <time.h>

#define BROJ_SEM 4

int SemId; // identifikacijski broj skupa semafora
int Id;   // identifikacijski broj segmenta
int *brojPosjetitelja;   // definiranje kazaljki na zajedničke procese, služi kao odbrojavanje za sve procese
int bP;  // za for petlju za stvaranje procesa

void SemGet(int n) 
{
		SemId = semget(IPC_PRIVATE, n, 0600);
		if (SemId == -1) {
				printf("Nema semafora!\n");
				exit(1);
		}
		printf("Postavili smo %d semafora\n", n);
		return;
}

int SemSetVal(int SemNum, int SemVal)
{
		return semctl(SemId, SemNum, SETVAL, SemVal);
}

int SemOp(int SemNum, int SemOp)
{
		struct sembuf SemBuf;
		SemBuf.sem_num = SemNum;
		SemBuf.sem_op = SemOp;
		SemBuf.sem_flg = 0;
		return semop(SemId, &SemBuf, 1);
}

void SemRemove(void) 
{
		(void) semctl(SemId, 0, IPC_RMID, 0);
		return;
}

void brisi(int sig) 
{
		// oslobadanje zauzete zajednicke memorije i brisanje semafora
		(void) shmdt((int *) brojPosjetitelja);  // otpuštanje segmenta A
		(void) shmctl(Id, IPC_RMID, NULL);  // uništavanje segmenta zaj. mem.
		printf("Uništena je zajednička memorija\n");
		SemRemove();
		printf("Uništeni su svi semafori!\n");
		exit(0);
}

void posjetitelj(int brojProcesa) {
		printf("Proces %d čeka u redu\n", brojProcesa);
		SemOp(0,-1); // proces ulazi
		printf("Proces %d je sjeo!\n", brojProcesa);
		SemOp(1,+1); // proces sjeo
		
		SemOp(2,-1); // cekamo da se napuni vrtuljak
		printf("Proces %d je ustao!\n", brojProcesa); 
		
		SemOp(3,+1); // javljamo da je ustao
		return;
}

void vrtuljak(int brojMjesta) 
{
		while (*brojPosjetitelja >= brojMjesta) {
				int i;
				for (i = 0; i < brojMjesta; i++) {
						SemOp(0,+1);
				}
				SemOp(1,-brojMjesta);  // cekamo da svi udu prije nego pokrenemo
				
				printf("Pokrećemo vrtuljak!\n");
				sleep(2);
				printf("Zaustavljamo vrtuljak... :(\n");
				
				SemOp(2,brojMjesta);
				
				printf("Svi su izašli, možemo dalje!\n");
				SemOp(3,-brojMjesta);
				*brojPosjetitelja -= brojMjesta;
				printf("Svi su sišli\n\n");
		}
		printf("Nema vas dovoljno za vožnju, vrtuljak se zatvara!\n");
		return;
}
			
int main(void) {
		Id = shmget(IPC_PRIVATE, sizeof(int), 0600);  // zauzimanje zajednicke memorije	
		if (Id == -1)
				exit(1);   // greska - nema zajednicke memorije

		brojPosjetitelja = (int *) shmat(Id, NULL, 0); // veze segment na zajednicku memoriju
		printf("Unesi broj prosjetitelja! :\n");
		scanf("%d", brojPosjetitelja);
		
		int brojMjesta;
		printf("Koliko najviše posjetitelja smije na vrtuljak? :) :\n");
		scanf("%d", &brojMjesta);
		
		SemGet(BROJ_SEM);
		int i;
		for (i = 0; i < BROJ_SEM; i++) 
				SemSetVal(i,0);
			
		sigset(SIGINT, brisi);   // u slucaju prekida briši memoriju i semafore
		
		bP = *brojPosjetitelja;
		
		if (fork() == 0) {
				sleep(5);
				vrtuljak(brojMjesta);  // vrtuljak je obavio svoj posao
				exit(0);
		}
		
		for (i = 1; i <= (bP - (bP % brojMjesta)); i++) {
				if (fork() == 0) {
						posjetitelj(i);
						exit(0);  // proces dijete tj. posjetitelj je napravio svoj posao
				}
		}
		
		i++;
		while (i--) wait (NULL);   // docekujemo sve procese dijete
		printf("Vrtuljak je zatvoren! :(\n");
		
		brisi(0);
	
		return 0;
}