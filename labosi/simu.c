#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void printNum(int n) {		// funkcija za ispis brojeva od 0 do 9
		int i;
		for (i = 1; i <= n; i++) {
				printf("%d", i % 10);
		}
		printf("\n");
		return;
}

void printMem(char* arr, int n) {		// funkcija za ispis memorije
		int i;
		for (i = 0; i < n; i++) {
				printf("%c", arr[i]);
		}
		printf("\n\n");
		return;
}

int zahtjev(char* arr, int brZahtjeva, int brLokacija, int n) {
		printf("Novi zahtjev %d za %d spremničkih mjesta\n", brZahtjeva, brLokacija);
		
		int i,j;
		int ukupanBrPraznih = 0;	// ukupan broj praznih mjesta u memoriji
		int brPraznih;				// broj mjesta u rupama
		int minBrPraznih = 0;		// najmanja rupa
		int indeks;					// indeks najmanje rupe
		
		for (i = 0; i < n; i++) {					// trazimo rupe
				if (arr[i] == '-') {				// ako smo naisli na rupu
						ukupanBrPraznih += 1;
						brPraznih = 1;
						
						for (j = i+1; j < n; j++) {
								if (arr[j] == '-') {
										brPraznih += 1;
										ukupanBrPraznih += 1;
								}
								else 
										break;
						}
						
						if ((minBrPraznih == 0 || brPraznih < minBrPraznih) && brPraznih >= brLokacija) {
								minBrPraznih = brPraznih;
								indeks = i;
						}
						i = j;		// preskacemo rupu
				}
		}
		
		if (ukupanBrPraznih < brLokacija) {		// nema dovoljno praznih mjesta, cak i ako preslozimo spremnik
				printf("Nema dovoljno mjesta u memoriji\n");
				return 0;
		}
		
		if (minBrPraznih != 0) {			// pronasli smo adekvatnu rupu
				char stringBrZahtjeva[10];
				for (i = indeks; i < indeks + brLokacija; i++) {
						sprintf(stringBrZahtjeva, "%d", brZahtjeva);
						arr[i] = stringBrZahtjeva[0];
				}
				return 1;
		}
		
		if (ukupanBrPraznih >= brLokacija) {  		 // nema adekvatne rupe, preslazemo spremnik
				int i, j = 0;
				char preslozeno[n];
				for (i = 0; i < n; i++) 
						preslozeno[i] = '-';
					
				for (i = 0; i < n; i++) {
						if (arr[i] != '-') {
								preslozeno[j] = arr[i];
								j++;
						}
				}
				char stringBrZahtjeva[10];
				for (i = j; i < j + brLokacija; i++) {
						sprintf(stringBrZahtjeva, "%d", brZahtjeva);
						preslozeno[i] = stringBrZahtjeva[0];
				}
				
				for (i = 0; i < n; i++) 
						arr[i] = preslozeno[i];
				return 1;
		}
}

void oslobadanje(char* arr, int brZahtjeva, int n) {
		printf("Oslobađa se zahtjev %d\n", brZahtjeva);
		
		char stringBrZahtjeva[10];
		sprintf(stringBrZahtjeva, "%d", brZahtjeva);
		int i;
		for (i = 0; i < n; i++) {				// trazimo rupe
				if (arr[i] == stringBrZahtjeva[0]) {
						arr[i] = '-';
				}
		}
}

int main(int argc, char *argv[]) {
		int N = atoi(argv[1]);
		char mem[N];
		int i;
		for (i = 0; i < N; i++) 
				mem[i] = '-';
			
		printNum(N);
		printMem(mem, N);
		
		char operacija;
		int broj;
		int brNovogZahtjeva = 0;
		printf("Za zahtjev upiši Z, za oslobađanje upiši O i redni broj zahtjeva: \n\n");
		
		while (1) {
				scanf("%c", &operacija);
				
				if (operacija == 'O') {
						printf("Koji zahtjev zelis osloboditi?\n");
						scanf("%d", &broj);
						oslobadanje(mem, broj, N);
						printNum(N);
						printMem(mem, N);
				}
					
				if (operacija == 'Z') {
						srand((unsigned)time(NULL));          // odabir broja lokacija
						broj = rand() % 7 + 1;
						int rez = zahtjev(mem, brNovogZahtjeva, broj, N);
					
						printNum(N);
						printMem(mem, N);
					
						if (rez == 1) {
								brNovogZahtjeva = (brNovogZahtjeva + 1) % 10;
						}		
				}
		}
		return 0;
}