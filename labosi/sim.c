#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void printNum(int n) {
		int i;
		for (i = 1; i <= n; i++) {
				printf("%d", i % 10);
		}
		printf("\n");
		return;
}

void printMem(char* arr, int n) {
		int i;
		for (i = 0; i < n; i++) {
				printf("%c", arr[i]);
		}
		printf("\n\n");
		return;
}

int zahtjev(char* arr, int brZahtjeva, int brLokacija, int n) {
		printf("Novi zahtjev %d za %d spremničkih mjesta\n", brZahtjeva, brLokacija);
		
		int i,j;
		int ukupanBrPraznih = 0;
		int brPraznih;				// broj mjesta u rupama
		int minBrPraznih = 0;		// najmanja rupa
		int indeks;					// indeks najmanje rupe
		for (i = 0; i < n; i++) {				// trazimo rupe
				if (arr[i] == '-') {				// ako smo naisli na rupu
						ukupanBrPraznih += 1;
						brPraznih = 1;
						for (j = i+1; j < n; j++) {
								if (arr[j] == '-') {
										brPraznih += 1;
										ukupanBrPraznih += 1;
								}
								else 
										break;
						}
						
						if ((minBrPraznih == 0 || brPraznih < minBrPraznih) && brPraznih >= brLokacija) {
								minBrPraznih = brPraznih;
								indeks = i;
						}
						i = j;		// preskacemo rupu
				}
		}
		
		if (ukupanBrPraznih < brLokacija) {
				printf("Nema dovoljno mjesta u memoriji\n");
				return 0;
		}
		
		if (minBrPraznih != 0) {		// pronasli smo adekvatnu rupu
				char stringBrZahtjeva[10];
				for (i = indeks; i < indeks + brLokacija; i++) {
						sprintf(stringBrZahtjeva, "%d", brZahtjeva);
						arr[i] = stringBrZahtjeva[0];
				}
				return 1;
		}
		
		if (ukupanBrPraznih >= brLokacija) {  		 // nema adekvatne rupe
				char preslozeno[n];
				int i, j = 0;
				
				for (i = 0; i < n; i++) 
						preslozeno[i] = '-';
	
				for (i = 0; i < n; i++) {
						if (arr[i] != '-') {
								preslozeno[j] = arr[i];
								j++;
						}
				}
				
				char stringBrZahtjeva[10];
				for (i = j; i < j + brLokacija; i++) {
						sprintf(stringBrZahtjeva, "%d", brZahtjeva);
						preslozeno[i] = stringBrZahtjeva[0];
				}
				for (i = 0; i < n; i++) 
						arr[i] = preslozeno[i];
				return 1;
		}
}

void oslobadanje(char* arr, int brZahtjeva, int n) {
		printf("Oslobađa se zahtjev %d\n", brZahtjeva);
		
		char stringBrZahtjeva[10];
		sprintf(stringBrZahtjeva, "%d", brZahtjeva);
		int i;
		for (i = 0; i < n; i++) {				// trazimo rupe
				if (arr[i] == stringBrZahtjeva[0]) {
						arr[i] = '-';
				}
		}
}

int main(int argc, char *argv[]) {
		int N = atoi(argv[1]);
		char mem[N];
		int i;
		for (i = 0; i < N; i++) 
				mem[i] = '-';
			
		printNum(N);
		printMem(mem, N);
		
		char operacija;
		int broj;
		int brNovogZahtjeva = 1;
		printf("Za zahtjev upiši Z i broj zahtjeva, za oslobađanje upiši O i redni broj zahtjeva: \n\n");
		
		while (1) {
				scanf("%c %d", &operacija, &broj);
				
			if (operacija == 'O') {
					oslobadanje(mem, broj, N);
					printNum(N);
					printMem(mem, N);
			}
					
			if (operacija == 'Z') {
					int rez = zahtjev(mem, brNovogZahtjeva, broj, N);
					
					printNum(N);
					printMem(mem, N);
					
					if (rez == 1) {
							brNovogZahtjeva = (brNovogZahtjeva + 1) % 10;
					}		
			}
		}
		
		return 0;
}