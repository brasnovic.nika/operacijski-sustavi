#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>


int Id1; /* identifikacijski broj segmenta */
int *ZajednickaVarijabla1;
int Id2; /* identifikacijski broj segmenta */
int *ZajednickaVarijabla2;
int SemId;


void SemGet(int n)
{  /* dobavi skup semafora sa ukupno n semafora */
   SemId = semget(IPC_PRIVATE, n, 0600);
   if (SemId == -1) {
      printf("Nema semafora!\n");
      exit(1);
   }
   printf("Dobili ste %d semafora!!\n", n);
}
int SemSetVal(int SemNum, int SemVal){ /* postavi vrijednost semafora SemNum na SemVal */
	printf("Stavljam vrijednost %d na semafor %d\n", SemVal, SemNum);
	return semctl(SemId, SemNum, SETVAL, SemVal);
}

int SemOp(int SemNum, int SemOp)
{  /* obavi operaciju SemOp sa semaforom SemNum */
   struct sembuf SemBuf;
   SemBuf.sem_num = SemNum;
   SemBuf.sem_op  = SemOp;
   SemBuf.sem_flg = 0;
   return semop(SemId, & SemBuf, 1);
}
void SemRemove(void)
{  /* uništi skup semafora */
   (void) semctl(SemId, 0, IPC_RMID, 0);
   printf("Brisem semafor\n");
}
void brisi(int sig)
{
   /* oslobađanje zajedničke memorije */
   (void) shmdt((char *) ZajednickaVarijabla1);
   (void) shmctl(Id1, IPC_RMID, NULL);
   (void) shmdt((char *) ZajednickaVarijabla2);
   (void) shmctl(Id2, IPC_RMID, NULL);
   printf("Brisem varijable\n");
   SemRemove();
   exit(0);
}
void Posjetitelji(int n){
	printf("Posjetitelj %d čeka da sjedne na vrtuljak\n", n);
	SemOp(1, -1);//posjetitelj je sjeo
    printf("Posjetitelj %d je sjeo i čeka da vožnja počne\n", n);
    SemOp(3, 1); //označava da je na vrtuljku ispunjeno još jedno mjesto
    SemOp(2, -1);//ljudi ne mogu ustati prije nego je vožnja gotova
    printf("Posjetitelj %d je ustao\n", n);
    SemOp(1, 1);//posjetitelj je ustao
}
void vrtuljak() {
    while (1) {
        SemOp(3, -(*ZajednickaVarijabla1)); //pokreće se tek kada je na vrtuljak sjelo N ljudi
        printf("Vožnja počinje!\n");
		sleep(9);
        printf("Vožnja je gotova!\n");
        SemOp(2, (*ZajednickaVarijabla1)); //označava da je vožnja gotova i dozvoljava ljudima da ustanu
    }

}

int main(void){
	int N, M, i, vrijeme_posjetitelja;
	int redni_broj=1;
	printf("Upiši broj ljudi na vrtuljku: ");
	scanf("%d", &N);
	printf("Upiši koliko sekundi će vrtuljak raditi u danu: ");
	scanf("%d", &M);
	srand((unsigned)time(NULL));
	Id1 = shmget(IPC_PRIVATE, sizeof(int), 0600);
	if (Id1 == -1){
		exit(1);  /* greška - nema zajedničke memorije */
	}
	ZajednickaVarijabla1 = (int *) shmat(Id1, NULL, 0);
	*ZajednickaVarijabla1 = N; //broj mjesta na vrtuljku 
	
	Id2 = shmget(IPC_PRIVATE, sizeof(int), 0600);
	if (Id2 == -1){
		exit(1);  /* greška - nema zajedničke memorije */
	}
	ZajednickaVarijabla2 = (int *) shmat(Id2, NULL, 0);
	*ZajednickaVarijabla2 = 0; //zastavica
	SemGet(4);
	printf("Broj mjesta: %d", *ZajednickaVarijabla1);
	SemSetVal(1,(*ZajednickaVarijabla1));
	SemSetVal(2,0);
	SemSetVal(3,0);
   
	sigset(SIGINT, brisi);
	
	if (fork()==0){
		vrtuljak();
		exit(1);
	}
	
	for(i=0; i<M; ){
		vrijeme_posjetitelja = rand()% (5-1+1)+1;
		sleep(vrijeme_posjetitelja);
		printf("%d . posjetitelj ulazi (nakon %d sekundi)\n", redni_broj, vrijeme_posjetitelja );
		fflush(stdout);
		i=i+vrijeme_posjetitelja;
		redni_broj++;
		if(fork()==0){
			Posjetitelji(redni_broj-1);
			exit(1);
		}
	}
	printf("Gotovo!");
	while (redni_broj--) wait (NULL);
	brisi(0);
	return 0;
	
}