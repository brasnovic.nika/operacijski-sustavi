#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/wait.h>

static int opis[90];
static int broj_stapica=0;
static int broj_ciklusa=3;
pthread_mutex_t M;
pthread_cond_t uvjet[1];


typedef struct {
	int broj;
	int broj_ruku;
}filozofi;

void *filozof(void *args){
	int i;
	filozofi *f= args;
	for (i=0; i<broj_ciklusa;i++){
		printf("Filozof %d ceka za uzimanje %d stapica !\n", f->broj, f->broj_ruku);
		Uzmi_stapice(f->broj_ruku);
		printf("Filozof %d jede!\n", f->broj);
		sleep(3);
		Spusti_stapice(f->broj_ruku);
		printf("Filozof %d je spustio stapice!\n", f->broj);
		printf("\nFilozof %d misli %d . put!\n", f->broj, i+1);
		sleep(3);
	}
	printf("\nFilozof %d je gotov sa konferencijom!\n", f->broj);
}
void Uzmi_stapice(int broj_ruku){
	pthread_mutex_lock(&M);
	while(broj_ruku>broj_stapica){
		pthread_cond_wait(&uvjet[0], &M);
	}
	broj_stapica=broj_stapica-broj_ruku;
	pthread_mutex_unlock(&M);
}

void Spusti_stapice(int broj_ruku){
	pthread_mutex_lock(&M);
	broj_stapica=broj_stapica+broj_ruku;
	pthread_cond_broadcast(&uvjet[0]);
	pthread_mutex_unlock(&M);
}

void brisi(){
	for(int i=0; i<2; i++){
		pthread_cond_destroy(&uvjet[i]); 
	}
	pthread_mutex_destroy(&M);
}

int init(){
	if(pthread_mutex_init(&M, NULL) != 0){
			printf("Monitor nije uspjesno inicijaliziran!\n");
			return 1;
	}
	
	if(pthread_cond_init(&uvjet[0], NULL) != 0){
				return 1;
	
	}
	return 0;
}

int main(void){
	int i, broj_filozofa, brojac=1, N;
	int *Broj = &N;
	int nums[30]={8,12,1,3,2,4,3,5,4,2,5,4,6,3,7,5,8,1};
	filozofi opis[30];
	pthread_t thr_id[30];
	sigset(SIGSTOP, brisi);
	
	printf("Ukupno je %d filozofa i %d stapica : \n", nums[0], nums[1]);
	broj_stapica=nums[1];
	broj_filozofa=nums[0];
	for (i=2; i<((broj_filozofa*2)+2) ;i=i+2){
		opis[brojac].broj= nums[i];
		opis[brojac].broj_ruku=nums[i+1];
		printf("Filozof %d , ima %d ruke/ruku\n", nums[i], nums[i+1]);
		brojac++;
	}
	printf("Odredite koliko ciklusa jesti- misliti će biti: ");
	scanf("%d", &broj_ciklusa);
	if(init()==1){
		printf("Inicijalizacija monitora i uvjetnih redova nije uspjela!\n");
	}
	
	for ( i = 1 ; i<broj_filozofa+1 ; i++ ){
		if( pthread_create( &thr_id[i] , NULL , *filozof , &opis[i]) != 0 ){
			printf( "Greška pri stvaranju dretve!! :(" );
		}
	}
	
	for(int i=0; i<30; i++){
		pthread_join(thr_id[i], NULL);
	}
	
	brisi();
	printf("Konferencija je gotova, hvala svim filozofima na sudjelovanju!\n");
	
	return 0;
	
}