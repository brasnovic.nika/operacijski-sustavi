#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
int Id; /* identifikacijski broj segmenta */
int *A;
 
void Povecaj(int broj){
   while(broj--){
	   *A=*A+1;
   }
   printf("%d \n", *A);
}

void brisi(int sig){
	
   (void) shmdt((char *)A);
   (void) shmctl(Id, IPC_RMID, NULL);
   
   exit(0);
}

int main( int argc, char *args[] ){
	int N ;
	int M ;
	int i ;
	int br;
	N = atoi( args[1] );
	M = atoi( args[2] );
	br = 0;
	printf( "%d  %d \n" , N , M );
	Id = shmget( IPC_PRIVATE , sizeof( int ), 0600 );
	if ( Id == -1 ){
		printf( "Greška - nema zajedničke memorije" );
		exit(1); 
	}
	
	A = (int *)shmat(Id, NULL, 0);
	*A = 0;
	sigset(SIGINT, brisi);//u slučaju prekida briši memoriju
 
   /* pokretanje paralelnih procesa */
	for( i=0 ; i<N ; i++ ){
		if (fork() == 0) {
		Povecaj(M);
		exit(0);
		}
	}
	   
	while (i--) {
		wait (NULL);
	}
	printf("A = %d\n", *A);
	brisi(0);
	
 
   return 0;
}