#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>

unsigned long zadnji= 1000000001;
int pauza=0;
struct itimerval t;

int prost ( unsigned long n ) {
	unsigned long i;
	double max = (sqrt(n*1.0));

	if ( ( n & 1 ) == 0 )
		return 0;
	
	for ( i = 3; i <= max; i += 2 )
		if ( ( n % i ) == 0 )
			return 0;

	return 1; 
}

void periodicki_ispis ( int sig )
{
	printf ( "zadnji prosti broj = %ld \n", zadnji );
}
void periodicki_ispis2 ( int sig )
{
	
}

int postavi_pauzu (int sig) {
   pauza = 1 - pauza;
   if (pauza==1){
	   sigset ( SIGALRM, periodicki_ispis2 );
   }
   else {
	   sigset ( SIGALRM, periodicki_ispis );
   }
}
void prekid (int sig) {
	printf("zadnji prosti broj = %ld \n", zadnji);
	exit(0);
}

int main ()
{
	
	unsigned long broj = 1000000001;

	sigset ( SIGALRM, periodicki_ispis );
	sigset ( SIGINT, postavi_pauzu);
	sigset ( SIGTERM, prekid);

	t.it_value.tv_sec = 5;
	t.it_value.tv_usec = 500000;
	t.it_interval.tv_sec = 5;
	t.it_interval.tv_usec = 500000;

	setitimer ( ITIMER_REAL, &t, NULL );

	while(1) {
		if ( prost ( broj ) == 1 ){
			zadnji = broj;
		}
		broj++;
		while( pauza == 1 ){
			wait(NULL);
		}
	}
	return 0;
}