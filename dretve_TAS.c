#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#define TAS(ZASTAVICA) __atomic_test_and_set (&ZASTAVICA, __ATOMIC_SEQ_CST)
 
int A;
int zastavica = 0 ;

void *Povecaj(void *x){
	int br;
	br = *((int*)x);
	while(br--){
		zastavica = 1;
		A++;
		zastavica = 0;
	}
	pthread_exit(x);
}


int main( int argc, char *args[] ){
	int N ;
	int M ;
	int i;
	A = 0;
	pthread_t thr_id[N];
	int *Broj = &M;
	N = atoi( args[1] );
	M = atoi( args[2] );
	printf( "%d  %d \n" , N , M );
	for ( i = 0 ; i<N ; i++ ){
		while( TAS(zastavica) != 0){
			wait(NULL);
		}
		if( pthread_create( &thr_id[i] , NULL , Povecaj , Broj) != 0 ){
			printf( "Greška pri stvaranju dretve!! :(" );
		}
		pthread_join(thr_id[i], NULL);
	}
	
	printf( "A = %d\n" , A );
	
	return 0;
}


