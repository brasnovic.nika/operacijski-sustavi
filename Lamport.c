#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <values.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
static int N;
static int *proces;
int *TRAZIM;
int *BROJ;
int ID;
int *A;


void brisi (int signal){
	(void)shmdt((int*)TRAZIM);
	(void)shmdt((int*)BROJ);
	(void)shmctl(ID, IPC_RMID, NULL);
	exit(0);
}

void Udi_U_KO(int i){
	int j;
	TRAZIM[i] = 1;
	int maxi= BROJ[0];
	for(int j = 1; j < N; j++){
			if(BROJ[j] > maxi){
				maxi = BROJ[j];
			}
		}
	BROJ[i] = maxi + 1;
	TRAZIM[i] = 0;
	for(j = 0; j<N ; j++){
		while(TRAZIM[j] != 0){
			wait(NULL);
		}
		while(BROJ[j] != 0 && (BROJ[j] < BROJ[i] || (BROJ[j] == BROJ[i] && j < i))){
			wait(NULL);
		}
	}
}

void Izadi_iz_KO(int i){
   BROJ[i] = 0;
}

void Povecaj(int i, int broj){
	while(broj--){
		Udi_U_KO(i);
		*A=*A+1;
		Izadi_iz_KO(i);
   }
   printf("%d \n", *A);
}

int main( int argc, char *args[] ){
	int N ;
	int M ;
	int i ;
	int br;
	N = atoi( args[1] );
	M = atoi( args[2] );
	sigset(SIGINT, brisi);
	printf( "%d  %d \n" , N , M );
	ID = shmget( IPC_PRIVATE , sizeof( int ), 0600 );
	if ( ID == -1 ){
		printf( "Greška - nema zajedničke memorije" );
		exit(1); 
	}
	proces = malloc(sizeof(int) * N);
	BROJ = (int*)shmat(ID, NULL, 0) + (N+1)*sizeof(int);
	TRAZIM = (int*)shmat(ID, NULL, 0);
	BROJ = (int*)shmat(ID, NULL, 0) + (N+1)*sizeof(int);
	A = (int *)shmat(ID, NULL, 0);
	*A = 0;
	sigset(SIGINT, brisi);//u slučaju prekida briši memoriju
	
	for(i=0; i<N; i++){
		TRAZIM[i] = 0;
		BROJ[i] = 0;
		proces[i] = i+1;
	}
 
   /* pokretanje paralelnih procesa */
	for( i=0 ; i<N ; i++ ){
		switch(fork()){
			case -1:
				fprintf(stderr, "Neuspjelo stvaranje procesa.\n");
				exit(1);
			case 0:
				Povecaj(proces[i], M);
				exit(0);
		}
	}
	   
	while (i--) {
		wait (NULL);
	}
	printf("A = %d\n", *A);
	brisi(0);
	
 
   return 0;
}