#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>



int sig[]={SIGCHLD, SIGBUS, SIGTERM, SIGFPE};
int pid=0;

void prekidna_rutina(int sig){
	kill(pid,SIGKILL);	
	exit(0);
}

int main(int argc, char *argv[]){
	int broj;	
	pid=atoi(argv[1]);
	sigset(SIGINT, prekidna_rutina);
	srand((unsigned)time(NULL));

	while(1){
		sleep(rand()%(5-3+1)+3);
		broj=rand()%(3-0+1)+0;
		kill(pid, sig[broj]);
	}
	return 0;
}
