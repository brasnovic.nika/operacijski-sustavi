#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <math.h>
#include <unistd.h>
#include <sys/time.h>

int pauza = 0;
int number = 1000000001;
int zadnji = 1000000001;

void periodicki_ispis(int sig){
	printf("Zadnji prosti broj: %d\n", zadnji);
}
int postavi_pauzu(int sig){
	pauza = pauza-1;
	return pauza;
}
void prekini(int sig){
	printf("Zadnji prosti broj: %d\n", zadnji);
	printf("Terminated");
	exit(0);
}
int prosti_broj(unsigned long n){
	unsigned long int i, max;
	if((n&1)==0){
		return 0;
	}
	max = sqrt(n);
	for(i=3;i<=max;i+=2){
		if(n%i==0){
			return 0;
		}
	}
	return 1;
}
int main(void){
	sigset(SIGINT,postavi_pauzu);
	sigset(SIGTERM, prekini);
	sigset(SIGALRM, periodicki_ispis);
	struct itimerval t;
	t.it_value.tv_sec = 5;
	t.it_value.tv_usec = 0;
	t.it_interval.tv_sec=5;
	t.it_interval.tv_usec = 0;
	setitimer(ITIMER_REAL, &t, NULL);
	while(1){
		if(prosti_broj(number)){
			zadnji=number;
		}
		++number;
		while(pauza){
			pause();
		}
	}
	return 0;
}
