#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void ispisi_spremnik(char *arr, int size){
	int i;
	for (i=1; i<=size; i++){
		printf("%d", (i%10));
	}
	printf("\n");
	for (i=0; i<size; i++){
		if(*(arr+i)=='-'){
			printf("%c", *(arr+i));
		}
		else printf("%d", (int)(*(arr+i)));
		
	}
	printf("\n");
	return;
}
void postavi_spremnik(char *arr, int size){
	
	int i;
	for(i=0; i<size; i++){
		*(arr+i)='-';
	}
	printf("Postavljen je prazan spremnik!\n");
	return;
}
void dodaj_u_spremnik(char *arr, int size, int redni_broj1){
	int i=0 , j=0 , mini=0 , indeks_mini=0 , ovaj=0 , k=0, z=0;
	int novi=rand()%(6-1+1)+1;

	printf("\nDodajem %d mjesta u spremnik.\n", novi);
	while(i<size){
		ovaj=0;
		k=0;
		if (*(arr+i)=='-'){
			k=i;
			while(*(arr+k)=='-'){
				ovaj++;
				k++;
			}
			if ( ( z==0 ) && ( ovaj >= novi ) ){
				printf("Evo mjesta");
				mini = ovaj;
				indeks_mini = i;
				z=1;
			}
			if( (ovaj<mini) && (ovaj>=novi)){
				printf("Evo mjesta");
				mini=ovaj;
				indeks_mini=i;
			}
			i = k ;
		}
		else i++ ;
	}
		
	if ( mini != 0 ){
		printf("Pronasao sam mjesto\n");
		for (j=0; j<novi;j++){
			(*(arr+indeks_mini+j))=redni_broj1;
			printf("Dodajem : %d %c \n", (*(arr+i+j)), (*(arr+i+j)));
		}
		return;
	}
	
	printf("Nema mjesta\n");
	return;
	
}
void ukloni_iz_spremnika(char *arr, int size, int redni_broj_zahtjev){
	int i=0;
	
	for (i=0; i<size; i++){
		if(*(arr+i)==redni_broj_zahtjev){
			(*(arr+i))='-';
		}
	}
	return;
	
}

int main(void){
	int velicina;
	int unos=0;
	int redni_broj_zahtjev;
	char zeljena_operacija;
	char *spr;
	int redni_broj[20]={0}, redni_broj1;
	int n, l,  redni_broj_odabir=1, provjera=0;
	
	srand((unsigned)time(NULL));
	printf("Unesite velicinu spremnika: ");
	scanf("%d", &velicina);
	spr = (char *) malloc(velicina+1);
	postavi_spremnik(spr, velicina);
	ispisi_spremnik(spr, velicina);
	printf("Unesi željenu operaciju. Spremnik je prazan pa očekujem Z.\n");
	printf("Za kraj pritisnite G \n");
	zeljena_operacija=getchar();
	
	while(1){
		//printf("Upisite sifru operacije: O ili Z\n");
		scanf("%c", &zeljena_operacija);
		if (zeljena_operacija=='Z'){
			provjera=1;
			for (n=1; n<20;n++){
				if (redni_broj[n]==0){
					redni_broj_odabir=n;
					redni_broj[n]=1;
					break;
				}
			}
			//printf("Odabrani broj: %d", redni_broj_odabir);
			dodaj_u_spremnik( spr, velicina, redni_broj_odabir);
			ispisi_spremnik(spr, velicina);
		}
		
		else if ((zeljena_operacija=='O') && (provjera==1)){
			printf("Upisite koji segment treba ukloniti: \n");
			scanf("%d", &unos);
			ukloni_iz_spremnika(spr, velicina, unos);
			redni_broj[unos]=0;
			ispisi_spremnik(spr, velicina);
		}
		else if ((zeljena_operacija=='G')){
			printf("Simulacija je gotova!\n");
			return 0;
		}
		else if (provjera ==0){
			printf("Krivi unos, cekam Z.\n");		
		} 
	}
	
	
	return 0;
}